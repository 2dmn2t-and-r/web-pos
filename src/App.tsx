import React, { useEffect } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import { ThemeProvider } from "@mui/material/styles";
import theme from "./configs/theme";

import "./index.css";

import PrivateRouter from './customRouter/PrivateRouter';
import PageRender from './customRouter/PageRender';
import CustomerPage from './pages/CustomerPage'
import QRPage from './pages/QRPage'
import PaymentCustomerPage from './pages/PaymentCustomerPage'
import AuthenticateController from './controllers/AuthenticateController';
import AuthPage from './pages/AuthPage';
import { GET_USER } from './constants';
import { useDispatch, useSelector } from 'react-redux';

function App() {
  const { auth }: any = useSelector(state => state);
  const dispatch = useDispatch();

  useEffect(() => {
    AuthenticateController.getAuthenticateState((user) => {
      dispatch({ type: GET_USER, payload: user });
    });
  }, [dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
          <Router>
            <Route exact path="/" component={AuthPage} />
            <Route exact path="/menu" component={CustomerPage} />
            <Route exact path="/payment" component={PaymentCustomerPage} />
            <Route exact path="/table/:resId/:tableId" component={QRPage} />
            {
              auth &&
              <Switch>
                <PrivateRouter exact path='/:page/' component={PageRender} />
                <PrivateRouter exact path='/:page/:id' component={PageRender} />
              </Switch>
            }
          </Router>
      </div>
    </ThemeProvider>
  );
}

export default App;
