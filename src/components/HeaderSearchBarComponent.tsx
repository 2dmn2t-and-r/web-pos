import React from 'react'

import { Box, InputBase } from '@mui/material'
import SearchIcon from '@mui/icons-material/Search'

interface HeaderSearchBarProps {
  keyword: string
  handleChangeKeyword?: (e: any) => void
  handleSearchDishes?: (e: any) => void
}

const HeaderSearchBarComponent = (props: HeaderSearchBarProps) => {
  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        px: 1,
        py: '5px',
        mr: '8px',
        maxWidth: '420px',
        border: '1px solid',
        borderColor: 'textColor.main',
        borderRadius: '5px',
        backgroundColor: 'lightColor.main',
      }}
      >
      <Box onClick={props.handleSearchDishes}>
        <SearchIcon sx={{ color: 'primaryColor.main', mr: 1, cursor: 'pointer' }} />
      </Box>
      <InputBase
        placeholder="Search category or menu..."
        sx={{ width: '100%', fontSize: '12px' }}
        value={props.keyword}
        onChange={props.handleChangeKeyword}
        onKeyUp={props.handleChangeKeyword}
      />
    </Box>
  )
}

export default HeaderSearchBarComponent
