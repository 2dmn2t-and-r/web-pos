import React, { useState } from 'react'

import { Dialog, Grid, Typography, List, TextField } from '@mui/material'
import { useDispatch } from 'react-redux'
import ImageIcon from '@mui/icons-material/Image'

import ButtonComponent from './ButtonComponent'
import UserController from '../controllers/UserController'
import { CHANGE_DISH_FIELD } from '../constants'
interface ModalDishProps {
  id: string
  resId: string
  name: string
  price: number
  description: string
  status: 'Available' | 'Unavailable' | 'Delete'
  image: string
  handleCancel: () => void
  handleChangeDishField: (e: any) => void
  updateDish: () => Promise<void>
  createDish: () => Promise<void>
}

interface ModalDishMode {
  open: boolean
  mode: 'view' | 'edit' | 'create'
  info?: ModalDishProps
}

const ModalDish = (props: ModalDishMode) => {
  return (
    <Dialog open={props.open} maxWidth="md">
      {props.mode === 'view' && props.info && ViewDish(props.info)}
      {props.mode === 'edit' && props.info && EditDish(props.info)}
      {props.mode === 'create' && props.info && CreateDish(props.info)}
    </Dialog>
  )
}

export default ModalDish

const ViewDish = (props: ModalDishProps) => {
  return (
    <Grid
      container
      padding="20px"
      paddingLeft="10%"
      paddingRight="10%"
      maxWidth="md"
      minWidth="smx"
    >
      <Grid item>
        <Typography variant="h4" fontWeight="bold" marginBottom="5px">
          View Dish
        </Typography>
      </Grid>
      <Grid container columns={{ xs: 12 }}>
        <Grid item xs={3} textAlign="right">
          <Typography variant="body2" fontWeight="bold" padding="4px">
            ID:
          </Typography>
          <Typography variant="body2" fontWeight="bold" padding="4px">
            Name:
          </Typography>
          <Typography variant="body2" fontWeight="bold" padding="4px">
            Price:
          </Typography>
          <Typography variant="body2" fontWeight="bold" padding="4px">
            Description:
          </Typography>
        </Grid>
        <Grid item xs={5} textAlign="left">
          <Typography variant="body2" padding="4px">
            {props.id}
          </Typography>
          <Typography variant="body2" padding="4px">
            {props.name}
          </Typography>
          <Typography variant="body2" padding="4px">
            ${props.price}
          </Typography>
          <List sx={{ maxHeight: '400px', width: '90%', overflow: 'scroll' }}>
            <Typography
              variant="body2"
              fontWeight="light"
              align="justify"
              style={{ wordWrap: 'break-word' }}
              padding="0px"
            >
              {props.description}
            </Typography>
          </List>
        </Grid>
        <Grid item xs={4} textAlign="right">
          <img src={props.image} width="80%" alt="View" />
        </Grid>
      </Grid>
      <Grid container marginTop="15px">
        <Grid item xs={8} sm={9} md={10} textAlign="right">
          <ButtonComponent
            children="Cancel"
            size="medium"
            color="lightColor"
            variant="contained"
            onClick={props.handleCancel}
          />
        </Grid>
        <Grid item xs={4} sm={3} md={2} textAlign="right">
          <ButtonComponent
            children="Delete"
            size="medium"
            color="primaryColor"
            variant="contained"
          />
        </Grid>
      </Grid>
    </Grid>
  )
}

const EditDish = (props: ModalDishProps) => {
  const dispatch = useDispatch()
  return (
    <Grid
      container
      padding="20px"
      paddingLeft="10%"
      paddingRight="10%"
      maxWidth="md"
      minWidth="sm"
    >
      <Grid item>
        <Typography variant="h4" fontWeight="bold" marginBottom="5px">
          Edit Dish
        </Typography>
      </Grid>
      <Grid container columns={{ xs: 12 }}>
        <Grid item xs={3} textAlign="right">
          <Typography variant="body2" fontWeight="bold" padding="4px">
            ID:
          </Typography>
          <Typography variant="body2" fontWeight="bold" padding="4px">
            Name:
          </Typography>
          <Typography variant="body2" fontWeight="bold" padding="4px">
            Price:
          </Typography>
          <Typography variant="body2" fontWeight="bold" padding="4px">
            Description:
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Typography variant="body2" padding="4px">
            {props.id}
          </Typography>
          <TextField
            style={{ margin: '2px' }}
            fullWidth
            margin="none"
            inputProps={{ style: { fontSize: '14px', padding: '1.8px' } }}
            defaultValue={props.name}
            name="name"
            onChange={props.handleChangeDishField}
          />
          <TextField
            style={{ margin: '2px' }}
            fullWidth
            margin="none"
            inputProps={{ style: { fontSize: '14px', padding: '1.8px' } }}
            defaultValue={props.price}
            name="price"
            onChange={props.handleChangeDishField}
          />
          <TextField
            multiline
            maxRows={10}
            style={{ margin: '2px' }}
            fullWidth
            margin="none"
            inputProps={{ style: { fontSize: '14px', padding: '0px' } }}
            defaultValue={props.description}
            name="description"
            onChange={props.handleChangeDishField}
          />
        </Grid>
        <Grid item xs={4} textAlign="right">
          <input id="image-input-update" type="file" accept={'image/*'} hidden onChange={async (event) => {
            var files = event.target.files
            if (files != null) {
              if (files[0].type.split('/')[0] === 'image') {
                UserController.convertImageBase64(files[0]).then((img) => {
                  dispatch({ type: CHANGE_DISH_FIELD, payload: { key: 'image', value: img } })
                });
              }
            }
          }} />
          <img src={props.image} width="80%" alt="Update" />
          <ButtonComponent
            children="Change Image"
            size="medium"
            color="primaryColor"
            variant="contained"
            endIcon={<ImageIcon />}
            onClick={() => {
              var element = document.getElementById("image-input-update");
              if (element != null) {
                element.click();
              }
            }}
          />
        </Grid>
      </Grid>
      <Grid container marginTop="15px">
        <Grid item xs={8} sm={9} md={10} textAlign="right">
          <ButtonComponent
            children="Cancel"
            size="medium"
            color="lightColor"
            variant="contained"
            onClick={() => {
              props.handleCancel()
            }}
          />
        </Grid>
        <Grid item xs={4} sm={3} md={2} textAlign="right">
          <ButtonComponent
            children="Update"
            size="medium"
            color="primaryColor"
            variant="contained"
            onClick={props.updateDish}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}

const CreateDish = (props: ModalDishProps) => {
  const dispatch = useDispatch()
  const [img, setImg] = useState<string>("./upload.png")
  return (
    <Grid
      container
      padding="20px"
      paddingLeft="10%"
      paddingRight="10%"
      maxWidth="md"
      minWidth="smx"
    >
      <Grid item>
        <Typography variant="h4" fontWeight="bold" marginBottom="5px">
          Create Dish
        </Typography>
      </Grid>
      <Grid container columns={{ xs: 12 }}>
        <Grid item xs={3} textAlign="right">
          <Typography variant="body2" fontWeight="bold" padding="4px">
            Name:
          </Typography>
          <Typography variant="body2" fontWeight="bold" padding="4px">
            Price:
          </Typography>
          <Typography variant="body2" fontWeight="bold" padding="4px">
            Description:
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <TextField
            style={{ margin: '2px' }}
            name="name"
            onChange={props.handleChangeDishField}
            fullWidth
            margin="none"
            inputProps={{ style: { fontSize: '14px', padding: '1.8px' } }}
          />
          <TextField
            style={{ margin: '2px' }}
            fullWidth
            name="price"
            onChange={props.handleChangeDishField}
            margin="none"
            inputProps={{ style: { fontSize: '14px', padding: '1.8px' } }}
          />
          <TextField
            multiline
            name="description"
            onChange={props.handleChangeDishField}
            maxRows={10}
            style={{ margin: '2px' }}
            fullWidth
            margin="none"
            inputProps={{ style: { fontSize: '14px', padding: '0px' } }}
          />
        </Grid>
        <Grid item xs={4} textAlign="right">
          <input id="image-input" type="file" accept={'image/*'} hidden onChange={async (event) => {
            var files = event.target.files
            if (files != null) {
              if (files[0].type.split('/')[0] === 'image') {
                UserController.convertImageBase64(files[0]).then((img) => {
                  setImg(img);
                  dispatch({ type: CHANGE_DISH_FIELD, payload: { key: 'image', value: img } })
                });
              }
            }
          }} />
          <img src={img} width="80%" alt="Upload" />
          <ButtonComponent
            children="Upload Image"
            size="medium"
            color="primaryColor"
            variant="contained"
            endIcon={<ImageIcon />}
            onClick={() => {
              var element = document.getElementById("image-input");
              if (element != null) {
                element.click();
              }
            }}
          />
        </Grid>
      </Grid>
      <Grid container marginTop="15px">
        <Grid item xs={8} sm={9} md={10} textAlign="right">
          <ButtonComponent
            children="Cancel"
            size="medium"
            color="lightColor"
            variant="contained"
            onClick={props.handleCancel}
          />
        </Grid>
        <Grid item xs={4} sm={3} md={2} textAlign="right">
          <ButtonComponent
            children="Create"
            size="medium"
            color="primaryColor"
            variant="contained"
            onClick={props.createDish}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}
