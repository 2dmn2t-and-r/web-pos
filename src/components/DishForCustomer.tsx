import React, { useState } from 'react'
import { Grid, Paper, Typography } from '@mui/material'

import { useSelector, useDispatch } from 'react-redux'

import ButtonComponent from './ButtonComponent'

import ModalProduct from './ModalProduct'

import { ADD_DISH } from '../constants'

interface DishForCustomerProps {
  id: string
  name: string
  price: number
  image: string
  description: string
}

const DishForCustomer = (props: DishForCustomerProps) => {

  const chosenDish = useSelector((state: any) => state.chosenDish)
  const dispatch = useDispatch()

  const [open, setOpen] = useState<boolean>(false)
  const handleOpen = (e: any) => {
    dispatch({ type: ADD_DISH, payload: { dish: { id: props.id, resId: '123', name: props.name, price: props.price, description: props.description, status: "Available", image: props.image }, amount: 1 } })
    setOpen(true)
  }
  
  return (
    <Paper sx={{ padding: '10px', width: '90%' }}>
      <Grid container justifyContent="space-around">
        <Grid item>
          <img src={props.image} width="100px" alt={props.name} />
        </Grid>
        <Grid item paddingLeft="5px">
          <Typography
            variant="body2"
            fontWeight="bold"
            textAlign="left"
            width="150px"
            height="45px"
          >
            {props.name}
          </Typography>
          <Grid container>
            <Grid item>
              <Typography variant="body2" textAlign="left" width="60px">
                {props.price}$
              </Typography>
            </Grid>
            <Grid item textAlign="right" width="130px">
              <ButtonComponent
                sx={{ px: '2px' }}
                children={
                  <Typography
                    variant="body2"
                    sx={{ fontSize: { xs: '12px', sm: '14px' } }}
                  >
                    Add to basket
                  </Typography>
                }
                size="small"
                color="primaryColor"
                variant="outlined"
                fullWidth={true}
                onClick={handleOpen}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <ModalProduct
        id={chosenDish.dish.id}
        name={chosenDish.dish.name}
        price={chosenDish.dish.price}
        description={chosenDish.dish.description}
        image={chosenDish.dish.image}
        isOpen={open}
        setOpen={setOpen}
      />
    </Paper>
  )
}

export default DishForCustomer
