import React from 'react'
import Loader from 'react-spinners/ClipLoader'

import { Box } from '@mui/material'

const Loading = () => {
  return (
    <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'center', minHeight: '100vh', minWidth: '100%'}}>
      <Loader color="#ef1a69" />
    </Box>
  )
}

export default Loading