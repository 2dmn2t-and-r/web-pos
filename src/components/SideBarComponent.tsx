import React from 'react'

import { Box, Typography } from '@mui/material'
import ButtonComponent from './ButtonComponent'

import logo from '../assets/images/logo.png'

interface SideBarProps {
  list: { icon: React.ReactNode; label: string, handleChangeLink?: () => void }[]
}

const SideBarComponent = (props: SideBarProps) => {
  return (
    <Box
      sx={{
        backgroundColor: 'primaryColor.main',
        maxWidth: '200px',
        p: 1,
        height: '100%',
        minHeight: '100vh'
      }}
    >
      <img
        src={logo}
        alt="Logo"
        style={{ maxWidth: '60%', display: 'block', margin: '5px auto 10px' }}
      />
      {props.list.map((item, index) => {
        return (
          <ButtonComponent
            sx={{
              my: 1,
              py: 1,
              boxShadow: 'none',
              justifyContent: 'flex-start',
            }}
            key={index}
            size="small"
            variant="contained"
            color="primaryColor"
            children={
              <Typography align="left" variant="body2">
                {item.label}
              </Typography>
            }
            startIcon={item.icon}
            fullWidth
            onClick={item.handleChangeLink}
          />
        )
      })}
    </Box>
  )
}

export default SideBarComponent
