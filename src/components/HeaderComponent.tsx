import React, { useState } from 'react'

import { Grid } from '@mui/material'
import { useDispatch } from 'react-redux'

import HeaderOptionComponent from './HeaderOptionComponent'
import HeaderSearchBarComponent from './HeaderSearchBarComponent'

import CustomerController from '../controllers/CustomerController'
import { SEARCH_DISHES, START_LOADING, END_LOADING } from '../constants'

interface HeaderProps {
  type: number
  handleChangeOptions: (index: number) => void
  menuList: string[]
}

const HeaderComponent = (props: HeaderProps) => {
  const [keyword, setKeyword] = useState<string>('')
  const dispatch = useDispatch()

  const handleSearchDishes = async () => {
    props.handleChangeOptions(0)
    dispatch({ type: START_LOADING })

    const data = await CustomerController.searchDish("123", keyword)
    dispatch({ type: SEARCH_DISHES, payload: data })

    dispatch({ type: END_LOADING })
  }
  const handleChangeKeyword = (e: any) => {
    if (e.keyCode === 13) {
      handleSearchDishes()
      return
    }
    setKeyword(e.target.value)
  }

  return (
    <Grid container sx={{ mt: 2 }}>
      <Grid item xs={12} alignSelf="center" sx={{ ml: { xs: '0', lg: '25%' } }}>
        <HeaderSearchBarComponent
          keyword={keyword}
          handleChangeKeyword={handleChangeKeyword}
          handleSearchDishes={handleSearchDishes}
        />
      </Grid>
      <Grid
        item
        sx={{
          mt: 4,
          p: 0,
          borderBottom: '1px solid',
          borderColor: 'textColor.main',
        }}
      >
        <HeaderOptionComponent
          menuList={props.menuList}
          type={props.type}
          handleChangeOptions={props.handleChangeOptions}
        />
      </Grid>
    </Grid>
  )
}

export default HeaderComponent
