import React, { useState } from 'react'

import { Box, Modal, Typography } from '@mui/material'
import ButtonComponent from './ButtonComponent'

interface ModalProps {
  content: string
  contentConfirmButton: string
  isOpen: boolean
}

const ModalComponent = (props: ModalProps) => {
  const [open, setOpen] = useState<boolean>(props.isOpen)
  const handleClose = () => setOpen(false)
  return (
    <div>
      <Modal open={open} onClose={handleClose}>
        <Box
          sx={{
            backgroundColor: 'lightColor.main',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 350,
            p: 2,
            borderRadius: '10px',
          }}
        >
          <Typography variant="h6" component="h2">
            <strong>Are you sure?</strong>
          </Typography>
          <Typography variant="body1" sx={{ fontSize: '12px', mt: 1, mb: 3 }}>
            {props.content}
          </Typography>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'flex-end',
            }}
          >
            <ButtonComponent
              size="small"
              color="textColor"
              children={<span>Cancel</span>}
              variant="outlined"
            />
            <ButtonComponent
              sx={{ boxShadow: 'none', ml: 2 }}
              size="small"
              color="primaryColor"
              children={<span>{props.contentConfirmButton}</span>}
              variant="contained"
            />
          </Box>
        </Box>
      </Modal>
    </div>
  )
}

export default ModalComponent
