import { Button, Grid, Typography, Box } from '@mui/material'
import React from 'react'

interface ClerkTableProps {
  id: string,
  status: 'Available' | 'Unavailable'
  handleChangeTableStatus: (id: string, status: 'Available' | 'Unavailable') => Promise<void>
}

const ClerkTable = (props: ClerkTableProps) => {
  return (
    <Box sx={{ width: '170px', height: '70px' }} border={1} borderRadius="10px">
      <Box
        sx={{
          width: '170px',
          height: '30px',
          backgroundColor: `${props.status === 'Available' ? '#A2F07B' : '#F7B2C5'}`,
          borderTopLeftRadius: '10px',
          borderTopRightRadius: '10px',
        }}
        textAlign="center"
      >
        <Grid container columns={2} textAlign="center">
          <Grid item width="70px" p="5px">
            <Typography variant="body2" fontWeight="bold" textAlign="left">
              Table {props.id}
            </Typography>
          </Grid>
          <Grid item width="100px" p="5px">
            <Typography variant="body2" fontWeight="bold" textAlign="right">
              {props.status}
            </Typography>
          </Grid>
        </Grid>
        <Button
          sx={{
            width: '130px',
            height: '30px',
            backgroundColor: 'black',
            borderTopLeftRadius: '0px',
            borderTopRightRadius: '0px',
            borderBottomLeftRadius: '10px',
            borderBottomRightRadius: '10px',
          }}
          variant="contained"
          onClick={() => props.handleChangeTableStatus(props.id, props.status)}
        >
          Change status
        </Button>
      </Box>
    </Box>
  )
}

export default ClerkTable
