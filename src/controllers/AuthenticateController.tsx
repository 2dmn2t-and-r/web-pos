import User from "../models/User";
import firebase from "../configs/firebase";
import UserController from "./UserController";
import "firebase/compat/auth";
import "firebase/compat/firestore";
import Restaurant from "../models/Restaurant";

export default class AuthenticateController extends UserController {
    private static user: User | null = null;
    private static isInit = false;

    private static async loadUser(user: firebase.User): Promise<User | null> {
        try {
            var userData = await firebase.firestore().collection("users").doc(user.uid).get();
            if (!userData.exists) {
                return await this.createUser(user);
            }
            else {
                var res: Restaurant | null = null;
                try {
                    if (userData.get('restaurant')) {
                        var resData = await (userData.get('restaurant') as firebase.firestore.DocumentReference).get();
                        if (resData.exists) {
                            res = new Restaurant(
                                resData.id,
                                resData.get('name'),
                                resData.get('staffEmail'),
                                resData.get('managerEmail')
                            );
                        }
                    }
                }
                catch {
                    res = null;
                }
                return new User(
                    userData.id,
                    userData.get('name') || user.displayName || '',
                    userData.get('image') || user.photoURL,
                    userData.get('role') || 'None',
                    userData.get('email') || user.email || '',
                    res,
                );
            }
        } catch (error) {
            return null;
        }
    }

    private static async createUser(user: firebase.User): Promise<User | null> {
        try {
            await firebase.firestore().collection("users").doc(user.uid).set({
                'name': user.displayName || '',
                'image': user.photoURL,
                'role': 'None',
                'email': user.email || '',
                'restaurant': null,
            });
            return new User(
                user.uid,
                user.displayName || '',
                user.photoURL,
                'None',
                user.email || '',
                null,
            );
        }
        catch (error) {
            return null;
        }
    }

    static getAuthenticateState(onReceive?: (user: User | null) => void): void {
        if (!this.isInit) {
            firebase.auth().onAuthStateChanged(async(curUser) => {
                if (curUser) {
                    const user = await this.loadUser(curUser);
                    if(user) {
                        this.isInit = true;
                        this.user = user
                        if (onReceive !== undefined) onReceive(user);
                    } else {
                        this.user = null
                        if (onReceive !== undefined) onReceive(null);
                    }   
                }
                else {
                    this.isInit = true;
                    if (onReceive !== undefined) onReceive(null);
                    this.user = null;
                }
            });
        }
    }

    static async login(): Promise<boolean> {
        try {
            if (!this.isInit) return false;
            var result = await firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider());
            var curUser = result.user;
            if (curUser !== null) {
                this.user = await this.loadUser(curUser);
                return true;
            }
            else {
                return false;
            }
        } catch (error) {
            this.logout();
            this.user = null;
            return false;
        }
    }

    static async logout(): Promise<boolean> {
        try {
            await firebase.auth().signOut();
            this.user = null;
            return true;
        } catch (error) {
            return false;
        }
    }

    static getUser(): User | null {
        return this.user;
    }
}