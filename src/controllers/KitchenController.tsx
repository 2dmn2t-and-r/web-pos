import Order from "../models/Order";
import Dish from "../models/Dish";
import Table from "../models/Table";
import UserController from "./UserController";
import "firebase/compat/firestore";
import firebase from "../configs/firebase";

export default class KitchenController extends UserController {
    static loadAcceptedOrders(resId: string, expiredTime: Date, onReceive?: (orders: Order[]) => void): void {
        let orders = firebase.firestore().collection('restaurants').doc(resId).collection('orders').where('status', '==', 'Accept');
        orders.onSnapshot(async (orderSnapShot) => {
            const orderList: Order[] = [];
            for (const order of orderSnapShot.docs) {
                if (order.data().createAt.seconds * 1000 + 86400000000 < expiredTime.getTime()) continue;
                const listOfDishes: { dish: Dish, amount: number, status: 'Complete' | 'Uncomplete' }[] = [];
                for (const dishItem of order.data().listOfDishes) {
                    const dishData = await dishItem.dish.get();
                    const dish = new Dish(
                        dishData.id,
                        resId,
                        dishData.data().name,
                        dishData.data().price,
                        dishData.data().description,
                        dishData.data().status,
                        dishData.data().image,
                    );
                    listOfDishes.push({
                        dish: dish,
                        amount: dishItem.amount,
                        status: dishItem.status,
                    })
                }
                const table: Table = new Table('', 'Available');
                const tableData = await order.data().table.get();
                table.setTable(
                    tableData.id,
                    tableData.data().status,
                );
                const orderItem: Order = new Order(
                    order.id,
                    order.data().createAt,
                    listOfDishes,
                    order.data().status,
                    order.data().owner,
                    table,
                    resId,
                );
                orderList.push(orderItem);
            }
            orderList.sort((item1: Order, item2: Order) => (
                item1.createAt > item2.createAt ? 1 : -1
            ));
            if (onReceive !== undefined) onReceive(orderList);
        }, error => {
            if (onReceive !== undefined) onReceive([]);
        });
    }

    static async finishDish(resId: string, orderId: string, dishId: string): Promise<boolean> {
        try {
            const order = await firebase.firestore().collection('restaurants').doc(resId).collection('orders').doc(orderId).get();
            const listOfDishes = order.data()?.listOfDishes;
            await Promise.all(
                listOfDishes.map(async (item: any) => {
                    const dish = await item.dish.get();
                    if (dish.id === dishId) {
                        item.status = 'Complete';
                    }
                })
            );
            console.log(listOfDishes)
            if (listOfDishes.find((item: any) => item.status === 'Uncomplete')) {
                await firebase.firestore().collection('restaurants').doc(resId).collection('orders').doc(orderId).update({
                    'listOfDishes': listOfDishes,
                });
            }
            else {
                await firebase.firestore().collection('restaurants').doc(resId).collection('orders').doc(orderId).update({
                    'listOfDishes': listOfDishes,
                    'status': 'Finish',
                });
            }
            return true;
        } catch {
            return false;
        }
    }
}