import Dish from "../models/Dish";
import Order from "../models/Order";
import Table from "../models/Table";
import Payment from "../models/Payment";
import "firebase/compat/firestore";
import firebase from '../configs/firebase'

export default class UserController {
    static async getMenu(resId: string): Promise<Dish[]> {
        try {
            var dishes: Dish[] = [];
            var query = await firebase.firestore().collection("restaurants").doc(resId).collection("dishes").where("status", "==", "Available").get();
            query.forEach((dish) => {
                dishes.push(new Dish(
                    dish.id,
                    resId,
                    dish.get("name"),
                    Number(dish.get("price")),
                    dish.get("description"),
                    dish.get("status"),
                    dish.get("image"),
                    dish.get("category") || ''
                ))
            });
            return dishes;
        }
        catch {
            return [];
        }
    }

    static async getMenuWithCategory(resId: string, category: string): Promise<Dish[]> {
        try {
            if (category === '') {
                return this.getMenu(resId);
            }
            var dishes: Dish[] = [];
            var query = await firebase.firestore().collection("restaurants").doc(resId).collection("dishes").where("status", "==", "Available").where("category", "==", category).get();
            query.forEach((dish) => {
                dishes.push(new Dish(
                    dish.id,
                    resId,
                    dish.get("name"),
                    Number(dish.get("price")),
                    dish.get("description"),
                    dish.get("status"),
                    dish.get("image"),
                    dish.get("category") || ''
                ))
            });
            return dishes;
        }
        catch {
            return [];
        }
    }

    static loadPayments(resId: string, onReceive?: (payments: Payment[]) => void): void {
        const payments = firebase.firestore().collection('restaurants').doc(resId).collection('payments').where('status', '==', 'Finish');

        payments.onSnapshot(async (paymentSnapShot) => {
            const paymentList: Payment[] = [];
            for (const payment of paymentSnapShot.docs) {
                const order = await payment.data().order.get();
                const listOfDishes: { dish: Dish, amount: number, status: 'Complete' | 'Uncomplete' }[] = [];
                for (const dishItem of order.data().listOfDishes) {
                    const dishData = await dishItem.dish.get();
                    const dish = new Dish(
                        dishData.id,
                        resId,
                        dishData.data().name,
                        dishData.data().price,
                        dishData.data().description,
                        dishData.data().status,
                        dishData.data().image,
                    );
                    listOfDishes.push({
                        dish: dish,
                        amount: dishItem.amount,
                        status: dishItem.status,
                    })
                }
                const table: Table = new Table('', 'Available');
                const tableData = await order.data().table.get();
                table.setTable(
                    tableData.id,
                    tableData.data().status,
                );
                const orderItem: Order = new Order(
                    order.id,
                    order.data().createAt,
                    listOfDishes,
                    order.data().status,
                    order.data().owner,
                    table,
                    resId,
                );
                const paymentItem = new Payment(
                    payment.id,
                    payment.data().createdAt,
                    orderItem,
                    payment.data().status,
                );
                paymentList.push(paymentItem);
            }
            paymentList.sort((item1: Payment, item2: Payment) => (
                item1.createAt > item2.createAt ? 1 : -1
            ));
            if (onReceive !== undefined) onReceive(paymentList);
        }, error => {
            if (onReceive !== undefined) onReceive([]);
        });
    }

    static loadTables(resId: string, onReceive?: (tables: Table[]) => void): void {
        let tables = firebase.firestore().collection('restaurants').doc(resId).collection('tables');
        tables.onSnapshot(async(tableSnapShot) => {
            const tableList: Table[] = [];
            for (const table of tableSnapShot.docs) {
                const tableItem = new Table(
                    table.id,
                    table.data().status,
                );
                tableList.push(tableItem);
            }
            tableList.sort((item1: Table, item2: Table) => (
                Number(item1.id) > Number(item2.id) ? 1 : -1
            ));
            if (onReceive !== undefined) onReceive(tableList);
        }, error => {
            if (onReceive !== undefined) onReceive([]);
        });
    }

    static async changeTableStatus(resId: string, tableId: string, status: string): Promise<Boolean> {
        try {
            await firebase.firestore().collection('restaurants').doc(resId).collection('tables').doc(tableId).update({
                'status': status,
            });
            return true;
        } catch {
            return false;
        }
    }

    static async convertImageBase64(image: File): Promise<string> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(image);
            reader.onloadend = () => resolve(reader.result as string);
            reader.onerror = (error) => reject("[Image Processing] " + error);
        });
    }
}