import {
  ADD_DISH_TO_ORDER,
  REMOVE_DISH_FROM_ORDER,
  MINUS_ORDER,
  PLUS_ORDER,
  CANCEL_ORDER,
  ORDER_UPDATE_ID,
  ORDER_UPDATE_STATUS_ACCEPT,
} from '../constants'

import Order from '../models/Order'
import Table from '../models/Table'

type ActionType = {
  type: string
  payload?: any
}

const initialState: Order = new Order('', new Date(), [], 'Not', '123', new Table('', 'Available'), '123');

const order = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case ADD_DISH_TO_ORDER: {
      const chosenDish = action.payload
      const check = state.listOfDishes.some(
        (item) => item.dish.id === chosenDish.dish.id
      )
      if (check) {
        return {
          ...state,
          listOfDishes: state.listOfDishes.map((item) => {
            if (item.dish.id === chosenDish.dish.id) {
              return {
                ...item,
                amount: item.amount + chosenDish.amount,
              }
            }
            return item
          })
        }
      } else {
        return {
          ...state,
          id: state.listOfDishes.length === 0 ? new Date().getTime().toString() : state.id,
          listOfDishes: [...state.listOfDishes, chosenDish],
        }
      }
    }
    case REMOVE_DISH_FROM_ORDER: {
      const id = action.payload
      return {
        ...state,
        listOfDishes: state.listOfDishes.filter((item) => item.dish.id !== id)
      }
    }
    case MINUS_ORDER: {
      const id = action.payload
      return {
        ...state,
        listOfDishes: state.listOfDishes.map(item => item.dish.id === id ? { ...item, amount: item.amount === 1 ? 1 : item.amount - 1 } : item),
      }
    }
    case PLUS_ORDER: {
      const id = action.payload
      return {
        ...state,
        listOfDishes: state.listOfDishes.map(item => item.dish.id === id ? { ...item, amount: item.amount + 1 } : item),
      }
    }
    case ORDER_UPDATE_ID: {
      return action.payload
    }
    case ORDER_UPDATE_STATUS_ACCEPT: {
      return {
        ...state,
        status: 'Accept',
      }
      }
    case CANCEL_ORDER: {
      return initialState
    }
    default:
      return state
  }
}

export default order
