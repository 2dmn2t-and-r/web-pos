import React from 'react'
import {useHistory} from 'react-router-dom'

import { Box, Typography } from '@mui/material'

import ButtonComponent from '../components/ButtonComponent'

import url from '../assets/images/error.webp'

const PageNotFound = () => {
  const history = useHistory()
  const handleBackToHome = () => {
    history.push('/')
  }
  return (
    <Box sx={{display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>
      <img src={url} alt="404" style={{ width: '50%', objectFit: 'cover' }} />
      <ButtonComponent
        sx={{marginTop: '20px'}}
        size="small"
        variant="contained"
        color="primaryColor"
        children={<Typography variant="body1">Trở về trang chủ</Typography>}
        onClick={handleBackToHome}
      />
    </Box>
  )
}

export default PageNotFound