import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import { Grid, Typography } from '@mui/material'

import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'

import DishForCustomer from '../components/DishForCustomer'
import HeaderComponent from '../components/HeaderComponent'
import PaginationComponent from '../components/PaginationComponent'
import CustomerOrder from '../components/CustomerOrder'
import ButtonIconComponent from '../components/ButtonIconComponent'
import ButtonComponent from '../components/ButtonComponent'
import Loading from '../components/Loading'

import {
  SEARCH_CATEGORY,
  GET_ALL_DISHES,
  START_LOADING,
  END_LOADING,
  CANCEL_ORDER,
} from '../constants'

import UserController from '../controllers/UserController'
import Dish from "../models/Dish"

const menuList: string[] = [
  'All',
  'Coffee',
  'Tea',
  'Juice',
  'Meal',
  'Snack',
  'Dessert',
]


const CustomerPage = () => {
  const [type, setType] = useState<number>(0)
  const [page, setPage] = useState<number>(1)
  const history = useHistory()
  const dispatch = useDispatch()

  const orderGlobal = useSelector((state: any) => state.order)
  const listOfDishes = useSelector((state: any) => state.dishes)
  const loadingGlobal = useSelector((state: any) => state.loading)

  useEffect(() => {
    const fetchData = async () => {
      dispatch({ type: START_LOADING })

      const dishes = await UserController.getMenu("123")
      dispatch({ type: GET_ALL_DISHES, payload: dishes })

      dispatch({ type: END_LOADING })
    }
    fetchData()
  }, [dispatch])

  const handleChangeToPayment = () => {
    history.push('/payment')
  }
  const handleChangeOptions: (index: number) => Promise<void> = async (index) => {
    setType(index)
    dispatch({ type: START_LOADING })
    switch (index) {
      case 0: {
        const dish = await UserController.getMenuWithCategory('123', '')
        dispatch({ type: SEARCH_CATEGORY, payload: dish })
        break
      }
      default: {
        const dish = await UserController.getMenuWithCategory('123', menuList[index])
        dispatch({ type: SEARCH_CATEGORY, payload: dish })
        break
      }
    }
    dispatch({ type: END_LOADING })
  }
  const handleCancelOrder = () => {
    dispatch({ type: CANCEL_ORDER })
  }
  const handleChangePage = (e: any, page: number) => {
    setPage(page)
  }

  return (
    <>
      <Grid container sx={{pl: '2%'}}>
        <Grid item xs={12} md={9}>
          <Grid container>
            <HeaderComponent type={type} handleChangeOptions={handleChangeOptions} menuList={menuList} />
            <Typography variant="body1" fontWeight="bold" marginY="10px">
              Choose dishes
            </Typography>
            {loadingGlobal ? <Loading /> :
              <>
                {listOfDishes.length === 0 ? <Grid item sx={{ margin: '20% auto' }}><Typography>Không tìm thấy sản phẩm mong muốn</Typography></Grid> :
                  <>
                    <Grid item container spacing={3}>
                      {listOfDishes.slice((page - 1) * 12, page * 12).map((item: Dish) => {
                        return (
                          <Grid item xs={12} sm={6} md={4} key={item.id}>
                            <DishForCustomer
                              id={item.id}
                              name={item.name}
                              price={parseFloat(item.price.toFixed(2))}
                              description={item.description}
                              image={item.image}
                            />
                          </Grid>
                        )
                      })}
                    </Grid>
                    <Grid item sx={{ margin: '0 auto', my: 6 }}>
                      <PaginationComponent
                        count={listOfDishes.length % 12 === 0 ? Math.floor(listOfDishes.length) / 12 : Math.floor(listOfDishes.length / 12) + 1}
                        page={page}
                        handleChangePage={handleChangePage}
                      />
                    </Grid>
                  </>}
              </>}
          </Grid>
        </Grid>
        {orderGlobal.listOfDishes.length > 0 && <Grid item sm={12} md={3} sx={{ minWidth: '200px' }}>
          <Grid
            item
            container
            paddingTop="20px"
            maxWidth="320px"
            justifyContent="space-between"
          >
            <Grid item>
              <Typography variant="body1" fontWeight="bold">
                Order list
              </Typography>
            </Grid>
            <Grid item>
              <ButtonIconComponent
                backgroundColor="lightColor"
                children={<ShoppingCartIcon />}
                size="small"
                color="primaryColor"
              ></ButtonIconComponent>
            </Grid>
          </Grid>
          <Grid item>
            <Typography variant="body2" color="textColor.main">
              # {orderGlobal.id}
            </Typography>
            <Typography
              variant="body2"
              fontWeight="bold"
              paddingTop="10px"
              marginBottom="10px"
            >
              Items
            </Typography>
          </Grid>
          <Grid
            item
            container
            maxHeight="450px"
            sx={{ overflowY: 'auto' }}
            spacing={0.5}
          >
            {orderGlobal.listOfDishes.length > 0 && orderGlobal.listOfDishes.map((item: any) => {
              return (
                <Grid item key={item.dish.id} xs={12}>
                  <CustomerOrder
                    id={item.dish.id}
                    name={item.dish.name}
                    price={parseFloat(item.dish.price.toFixed(2))}
                    amount={item.amount}
                  />
                </Grid>
              )
            })}
          </Grid>
          <Grid item maxWidth="320px" paddingTop='20px'>
            <ButtonComponent
              children={
                <Typography>Proceed</Typography>}
              sx={{ borderRadius: '10px' }}
              size='medium'
              color='primaryColor'
              variant='contained'
              fullWidth
              onClick={handleChangeToPayment}
            />
          </Grid>
          <Grid item maxWidth="320px" paddingTop='20px'>
            <ButtonComponent
              children={
                <Typography>Cancel</Typography>}
              sx={{ borderRadius: '10px' }}
              size='medium'
              color='lightColor'
              variant='contained'
              fullWidth
              onClick={handleCancelOrder}
            />
          </Grid>
        </Grid>}
      </Grid>

    </>
  )
}

export default CustomerPage
