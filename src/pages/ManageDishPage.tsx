import React, { useState, useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";

import { Grid, Typography } from "@mui/material";
import StaffHeader from "../components/StaffHeader";
import DishForManager from "../components/DishForManager";
import ButtonIconComponent from "../components/ButtonIconComponent";
import CreateNewFolderSharpIcon from "@mui/icons-material/CreateNewFolderSharp";
import HeaderSearchBarComponent from "../components/HeaderSearchBarComponent";
import ModalDish from "../components/ModalDish";
import PaginationComponent from "../components/PaginationComponent";
import Loading from "../components/Loading";
import SideBarForEmployee from "../components/SideBarForEmployee";

import {
  CHANGE_DISH_FIELD,
  GET_ALL_DISHES,
  VIEW_DISH,
  UPDATE_ONCE,
  CLEAR_CHOSEN_DISH,
  ADD_DISH_TO_LIST,
  REMOVE_DISH_FROM_LIST,
  START_LOADING,
  END_LOADING,
} from "../constants";

import ManagerController from "../controllers/ManagerController";
import Dish from "../models/Dish";

const ManageDishPage = () => {
  const [searchValue, setSearchValue] = useState("");
  const [dishMode, setDishMode] = useState<"view" | "edit" | "create">("view");
  const [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const dishesGlobal = useSelector((state: any) => state.dishes);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const chosenDishGlobal = useSelector((state: any) => state.chosenDish);
  const loadingGlobal = useSelector((state: any) => state.loading);
  const handleChangeDishField = async (e: any) => {
    dispatch({
      type: CHANGE_DISH_FIELD,
      payload: { key: e.target.name, value: e.target.value },
    });
  };
  const searchValueChangeHandler = (event: any) => {
    setSearchValue(event.target.value);
  };
  const handleCancel = () => {
    setIsOpen(false);
  };
  const handleChangePage = (e: any, page: number) => {
    setPage(page);
  };
  const handleViewDish = (id: string) => {
    setDishMode("view");
    const dishChosen = dishesGlobal.filter((i: Dish) => i.id === id)[0];
    dispatch({ type: VIEW_DISH, payload: { dish: dishChosen, amount: 0 } });

    setIsOpen(true);
  };
  const handleCreateDish = () => {
    dispatch({ type: CLEAR_CHOSEN_DISH });
    setIsOpen(true);
    setDishMode("create");
  };
  const handleEditDish = (id: string) => {
    setDishMode("edit");
    const dishChosen = dishesGlobal.filter((i: Dish) => i.id === id)[0];
    dispatch({ type: VIEW_DISH, payload: { dish: dishChosen, amount: 0 } });

    setIsOpen(true);
  };
  const updateDish = async () => {
    const check = await ManagerController.updateDish(chosenDishGlobal.dish);
    if (check) {
      dispatch({ type: UPDATE_ONCE, payload: { dish: chosenDishGlobal.dish } });
      setIsOpen(false);
    }
  };
  const createDish = async () => {
    const newDish = new Dish(
      new Date().toISOString(),
      "123",
      chosenDishGlobal.dish.name,
      chosenDishGlobal.dish.price,
      chosenDishGlobal.dish.description,
      "Available",
      chosenDishGlobal.dish.image
    );

    const check = await ManagerController.createDish(newDish);
    if (check) {
      dispatch({ type: ADD_DISH_TO_LIST, payload: newDish });
      setIsOpen(false);
    }
  };
  const deleteDish = async (id: string) => {
    const check = await ManagerController.deleteDish("123", id);
    if (check) {
      dispatch({ type: REMOVE_DISH_FROM_LIST, payload: { id } });
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      dispatch({ type: START_LOADING });

      const dishes = await ManagerController.getMenu("123");
      dispatch({ type: GET_ALL_DISHES, payload: dishes });

      dispatch({ type: END_LOADING });
    };
    fetchData();
  }, [dispatch]);

  if (loadingGlobal) {
    return <Loading />
  }

  return (
    <>
      <Grid container padding="0">
        <Grid item xs={2}>
          <SideBarForEmployee />
        </Grid>
        <Grid item xs={10} padding="20px">
          <Grid item marginBottom="10px">
            <StaffHeader page="Dishes Management" />
          </Grid>
          <Grid item>
            <Grid container padding="20px" justifyContent="space-between">
              <Grid item xs={5}>
                <HeaderSearchBarComponent
                  keyword={searchValue}
                  handleChangeKeyword={searchValueChangeHandler}
                />
              </Grid>
              <Grid item xs={1}>
                <ButtonIconComponent
                  py={0.1}
                  children={
                    <CreateNewFolderSharpIcon sx={{ fontSize: "32px" }} />
                  }
                  color="lightColor"
                  size="small"
                  backgroundColor="titleColor"
                  onClick={handleCreateDish}
                />
              </Grid>
            </Grid>
            <Grid container sx={{ backgroundColor: "#EDF2F7", height: "40px" }}>
              <Grid item xs={1} textAlign="center">
                <Typography sx={{ fontWeight: "600", lineHeight: "40px" }}>
                  ID
                </Typography>
              </Grid>
              <Grid item xs={2} textAlign="center">
                <Typography sx={{ fontWeight: "600", lineHeight: "40px" }}>
                  Name
                </Typography>
              </Grid>
              <Grid item xs={1} textAlign="center">
                <Typography sx={{ fontWeight: "600", lineHeight: "40px" }}>
                  Price
                </Typography>
              </Grid>
              <Grid
                item
                md={3}
                textAlign="center"
                sx={{ display: { xs: "none", lg: "block" } }}
              >
                <Typography sx={{ fontWeight: "600", lineHeight: "40px" }}>
                  Description
                </Typography>
              </Grid>
              <Grid item xs={2} textAlign="center">
                <Typography sx={{ fontWeight: "600", lineHeight: "40px" }}>
                  Image
                </Typography>
              </Grid>
              <Grid item xs={5} lg={2} textAlign="center">
                <Grid container justifyContent="space-evenly">
                  <Typography sx={{ fontWeight: "600", lineHeight: "40px" }}>
                    View
                  </Typography>
                  <Typography sx={{ fontWeight: "600", lineHeight: "40px" }}>
                    Edit
                  </Typography>
                  <Typography sx={{ fontWeight: "600", lineHeight: "40px" }}>
                    Delete
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            {dishesGlobal
              .slice((page - 1) * 12, page * 12)
              .map((item: Dish) => (
                <DishForManager
                  key={item.id}
                  id={item.id}
                  name={item.name}
                  price={item.price}
                  description={item.description}
                  image={item.image}
                  handleViewDish={handleViewDish}
                  handleEditDish={handleEditDish}
                  deleteDish={deleteDish}
                />
              ))}
          </Grid>
          <Grid item sx={{ margin: "12px auto 0 auto" }}>
            <PaginationComponent
              count={
                dishesGlobal % 12
                  ? Math.floor(dishesGlobal.length / 12) + 1
                  : Math.floor(dishesGlobal.length / 12)
              }
              handleChangePage={handleChangePage}
            />
          </Grid>
        </Grid>
        <ModalDish
          mode={dishMode}
          open={isOpen}
          info={{
            id: chosenDishGlobal.dish.id,
            resId: "123",
            name: chosenDishGlobal.dish.name,
            price: chosenDishGlobal.dish.price,
            description: chosenDishGlobal.dish.description,
            status: "Available",
            image: chosenDishGlobal.dish.image,
            handleCancel,
            handleChangeDishField,
            updateDish,
            createDish,
          }}
        />
      </Grid>
    </>
  );
};

export default ManageDishPage;
