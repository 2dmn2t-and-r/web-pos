import React from 'react'

import { Typography, Grid } from '@mui/material'

import GoogleIcon from '@mui/icons-material/Google'
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

import ButtonComponent from '../components/ButtonComponent'

import logo from '../assets/images/logo-2.png'

import AuthenticateController from '../controllers/AuthenticateController'
import { useHistory } from 'react-router'

const AuthPage = () => {
  const history = useHistory();

  const login = async () => {
    await AuthenticateController.login();
    history.push("/profile");
  }
  const backHome = () => {
    history.push('/menu')
  }

  return (
    <>
      <Grid container direction="column" alignItems="center" justifyContent="center" sx={{ minHeight: '100vh' }} rowSpacing={6}>
        <Grid item container justifyContent="center" alignItems="center" direction="column" sx={{ width: '45%' }}>
          <Grid container item alignItems="center" justifyContent="center">
            <img
              src={logo}
              alt="Logo"
              style={{ objectFit: 'cover', maxWidth: '45%' }}
            />
          </Grid>
          <Grid item>
            <ButtonComponent
              sx={{ borderRadius: '50px', mt: '20%', padding: '10px 50px' }}
              variant="contained"
              size="large"
              color="primaryColor"
              children={
                <Typography variant="h5" component="h5">
                  Login with Google
                </Typography>
              }
              startIcon={<GoogleIcon fontSize="inherit" />}
              onClick={login}
            />
          </Grid>
        </Grid>
        <Grid item>
          <ButtonComponent
            variant="contained"
            size="medium"
            color="primaryColor"
            children={<Typography>Home</Typography>}
            startIcon={<ArrowBackIcon />}
            onClick={backHome}
          />
        </Grid>
      </Grid>
    </>
  )
}

export default AuthPage
