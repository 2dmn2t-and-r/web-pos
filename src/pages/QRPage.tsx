import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useParams, useHistory } from 'react-router-dom'

import UserController from '../controllers/UserController'
import { SET_INFO } from '../constants'


const QRPage = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const { resId, tableId }: any = useParams()

    useEffect(() => {
        const setTableForCustomer = async () => {
            await UserController.changeTableStatus(resId, tableId, 'Unavailable')
            history.push('/menu')
        }
        setTableForCustomer()
        dispatch({ type: SET_INFO, payload: tableId })
    }, [resId, tableId, history, dispatch])

    return (
        <>
            <span>Scanning...</span>
        </>
    )
}

export default QRPage