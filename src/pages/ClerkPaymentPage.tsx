import React, { useEffect, useState } from 'react'
import { Grid } from '@mui/material'
import { useSelector } from 'react-redux';
import StaffHeader from '../components/StaffHeader';
import ClerkPayment from '../components/ClerkPayment';
import SideBarForEmployee from '../components/SideBarForEmployee'

import UserController from '../controllers/UserController'
import ClerkController from '../controllers/ClerkController';

import Payment from '../models/Payment';

const ClerkPaymentPage = () => {
    const auth = useSelector((state: any) => state.auth)
    const [paymentList, setPaymentList] = useState<Payment[]>([])

    const handleClerkConfirmPayment = async (id: string) => {
        await ClerkController.confirmPayment(auth.restaurant.id, id)
    }

    useEffect(() => {
        UserController.loadPayments(auth.restaurant.id, setPaymentList)
    }, [auth.restaurant.id])

    return (
        <Grid container>
            <Grid item xs={2}>
                <SideBarForEmployee />
            </Grid>
            <Grid item xs={10} padding='20px'>
                <Grid item marginBottom='10px'>
                    <StaffHeader page='Payment' />
                </Grid>
                <Grid item container direction='column' marginTop='10px' spacing={2}>
                    <Grid item>
                        {paymentList.map((item: Payment) => {
                            return (
                                <ClerkPayment
                                    key={item.id}
                                    id={item.id}
                                    listOfDishes={item.order.listOfDishes}
                                    handleClerkConfirmPayment={handleClerkConfirmPayment}
                                />
                            )
                        })}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default ClerkPaymentPage