import React, { useState, useEffect } from 'react'

import { useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

import { Grid, Typography } from '@mui/material'
import ButtonComponent from '../components/ButtonComponent'
import PaymentItem from '../components/PaymentItem'
import PricePayment from '../components/PricePayment'

import { CANCEL_ORDER, ORDER_UPDATE_STATUS_ACCEPT, ORDER_UPDATE_ID } from '../constants'

import CustomerController from '../controllers/CustomerController'
import Order from "../models/Order"
import Table from "../models/Table"

import PageNotFound from '../pages/PageNotFound'

const PaymentCustomerPage = () => {
  const dispatch = useDispatch()
  const [isDone, setIsDone] = useState<boolean>(false)
  const orderGlobal = useSelector((state: any) => state.order)
  const infoGlobal = useSelector((state: any) => state.info)

  const subTotal: number = orderGlobal.listOfDishes.reduce(
    (sum: number, item: any) => sum + item.amount * item.dish.price,
    0
  )
  const history = useHistory()
  const handleBackHome = () => {
    history.push('/menu')
    dispatch({ type: CANCEL_ORDER })
  }
  const handlePaymentDone = async () => {
    await CustomerController.finishPayment(orderGlobal.resId, orderGlobal.id)
    setIsDone(true)
    dispatch({ type: ORDER_UPDATE_STATUS_ACCEPT })
  }
  const [isActive, setIsActive] = useState<boolean>(false)

  useEffect(() => {

    const handlePaymentSendToBE = async () => {
      const newOrder = new Order(new Date().getTime().toString(), new Date(), orderGlobal.listOfDishes, 'Not', orderGlobal.owner, new Table('1', 'Available'), orderGlobal.resId)

      await CustomerController.sendOrder(newOrder, () => {
        setIsActive(true)
      })
      dispatch({ type: ORDER_UPDATE_ID, payload: newOrder })
    }
    handlePaymentSendToBE()

  }, [dispatch, orderGlobal.resId, orderGlobal.listOfDishes, orderGlobal.owner])

  if (orderGlobal.listOfDishes.length === 0) {
    return <PageNotFound />
  }

  return (
    <Grid
      container
      justifyContent="center"
      sx={{
        paddingX: { xs: '20px', sm: '40px', md: '80px' },
        paddingTop: { xs: '10px', sm: '20px', md: '40px' },
      }}
    >
      <Grid item>
        <Typography
          fontWeight="bold"
          textAlign="center"
          sx={{ fontSize: { xs: '25px', sm: '30px', md: '35px' } }}
        >
          Bill # {orderGlobal.id}
        </Typography>
        <Typography
          textAlign="center"
          color={isDone ? 'greenColor.main' : 'primaryColor.main'}
          sx={{ fontSize: { xs: '20px', sm: '25px', md: '30px' } }}
        >
          {isDone ? 'Accepted' : 'Not accepted'}
        </Typography>
      </Grid>
      <Grid
        item
        container
        justifyContent="space-between"
        alignItems="baseline"
        sx={{ marginTop: { xs: '10px', md: '20px' } }}
      >
        <Grid item>
          <Typography variant="h6" fontWeight="bold">
            Table {infoGlobal.tableId}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body1" fontWeight="bold" textAlign="center">
            Time: {orderGlobal.createAt.toString().replace('GMT+0700 (Indochina Time)', '')}
          </Typography>
        </Grid>
      </Grid>
      <Grid
        item
        container
        marginBottom="10px"
        paddingY="10px"
        sx={{
          border: '1px solid black',
          borderRadius: '10px',
          marginTop: { xs: '10px', md: '30px' },
        }}
      >
        <Grid item xs={2} />
        <Grid item xs={3}>
          <Typography variant="body2" fontWeight="bold" textAlign="center">
            Tên món án
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <Typography variant="body2" fontWeight="bold" textAlign="center">
            Giá tiền
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <Typography variant="body2" fontWeight="bold" textAlign="center">
            Số lượng
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="body2" fontWeight="bold" textAlign="center">
            Thành tiền
          </Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={2}>
        {orderGlobal.listOfDishes.map((item: any) => {
          return (
            <Grid item key={item.dish.id} xs={12}>
              <PaymentItem
                name={item.dish.name}
                price={item.dish.price}
                quantity={item.amount}
                image={item.dish.image}
              />
            </Grid>
          )
        })}
      </Grid>
      <Grid container justifyContent="flex-end" marginTop="20px">
        <PricePayment subTotal={subTotal} />
      </Grid>
      <Grid container justifyContent="space-between" marginY="20px">
        <Grid item>
          <ButtonComponent
            size="medium"
            color="primaryColor"
            variant="contained"
            children={
              <Typography sx={{ fontSize: { xs: '15px', md: '20px' } }}>
                Back to home
              </Typography>
            }
            onClick={handleBackHome}
          />
        </Grid>
        {!isDone ? <Grid item>
          <ButtonComponent
            size="medium"
            color="primaryColor"
            variant="contained"
            children={
              <Typography sx={{ fontSize: { xs: '15px', md: '20px' } }}>
                Payment
              </Typography>
            }
            onClick={handlePaymentDone}
            disabled={!isActive}
          />
        </Grid> : <></>}

      </Grid>
    </Grid>
  )
}

export default PaymentCustomerPage
