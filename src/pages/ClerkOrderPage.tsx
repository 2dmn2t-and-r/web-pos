import React, { useState, useEffect } from 'react'
import { Grid } from '@mui/material'

import { useSelector } from 'react-redux';
import StaffHeader from '../components/StaffHeader';
import ClerkOrder from '../components/ClerkOrder';
import SideBarForEmployee from '../components/SideBarForEmployee';

import ClerkController from '../controllers/ClerkController';
import Order from '../models/Order'

const ClerkOrderPage = () => {
    const [clerkOrder, setClerkOrder] = useState<Order[]>([])
    const auth = useSelector((state: any) => state.auth)

    const handleCheckOrder = async (id: string) => {
        await ClerkController.acceptOrder(auth.restaurant.id, id)
    }
    useEffect(() => {
        ClerkController.loadOrders(auth.restaurant.id, ['Not'], new Date(), setClerkOrder)
    }, [auth.restaurant.id])

    return (
        <Grid container>
            <Grid item xs={2}>
                <SideBarForEmployee />
            </Grid>
            <Grid item xs={10} padding='20px'>
                <Grid item marginBottom='10px'>
                    <StaffHeader page='Order' />
                </Grid>
                <Grid item container direction='column' marginTop='10px' spacing={2}>
                    {
                        clerkOrder.map((item: Order) => {
                            return (
                                <Grid item key={item.id}>
                                    <ClerkOrder
                                        checked={false}
                                        id={item.id}
                                        listOfDishes={item.listOfDishes}
                                        handleCheckOrder={handleCheckOrder}
                                    />
                                </Grid>
                            )
                        })
                    }
                </Grid>
            </Grid>
        </Grid>
    )
}

export default ClerkOrderPage